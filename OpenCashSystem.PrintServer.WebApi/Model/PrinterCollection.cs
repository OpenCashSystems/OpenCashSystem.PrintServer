﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThermPrint;

namespace OpenCashSystem.PrintServer.WebApi.Model
{
    /// <summary>
    /// JSon document
    /// </summary>
    public class PrinterCollection 
    {
        public List<int> PrinterId { get; set; }

        public PrinterCollection()
        {
            PrinterId = new List<int>();
        }
        public PrinterCollection(Dictionary<int, NetworkPrinter> printers)
        {
            PrinterId = printers.Keys.ToList();
        }
        
    }
}