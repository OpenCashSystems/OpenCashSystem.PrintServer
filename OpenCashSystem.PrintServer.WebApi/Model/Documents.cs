﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCashSystem.PrintServer.WebApi.Model
{
    /// <summary>
    /// Document to print
    /// </summary>
    public class Documents
    {
        public List<Dictionary<string, string>> ListOfDataItems { get; set; }
        public int PrinterId { get; set; }
        public string TemplateName { get; set; }
    }
}
