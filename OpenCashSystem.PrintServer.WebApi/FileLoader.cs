﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ThermPrint;

namespace OpenCashSystem.PrintServer.WebApi
{
    /// <summary>
    /// Load temp files
    /// </summary>
    public class FileLoader
    {
        public DirectoryInfo CurrentParent { get; }
        public DirectoryInfo Templates { get; set; }
        public DirectoryInfo Commands { get; private set; }

        public event EventHandler<FileInfo[]> OnFilesChanged;

        private Task listener;
        private CancellationTokenSource tokenSource;
        private int templateFileCount;
        private int commandsFileCount;


        public FileLoader()
        {
            CurrentParent = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory;
            Templates = new DirectoryInfo(Path.Combine(CurrentParent.FullName, "Templates"));
            Commands = new DirectoryInfo(Path.Combine(CurrentParent.FullName, "Commands"));
            tokenSource = new CancellationTokenSource();

            DefaultFileExport();

            listener = new Task(() => Listen(), tokenSource.Token, TaskCreationOptions.LongRunning);
        }

        public void Start() => listener.Start();

        public void Stop() => tokenSource.Cancel();

        public Dictionary<string, PrintCommand> GetCommands()
        {
            var parser = new PrintCommandParser();
            foreach (var file in Commands.GetFiles("*.json"))
            {
                parser.LoadFromFile(file.FullName);
            }

            return parser.PrintCommands;
        }

        public List<PrintTemplate> GetTemplates()
        {
            var tmpList = new List<PrintTemplate>();
            var reader = new TemplateReader(new PrintCommandParser(GetCommands()));

            foreach (var file in Templates.GetFiles("*.ptf"))
                tmpList.AddRange(reader.Read(file.FullName));

            return tmpList;
        }
        public Dictionary<string, PrintTemplate> GetTemplatesAsDictionary()
        {
            var tmpDictionary = new Dictionary<string, PrintTemplate>();
            var reader = new TemplateReader(new PrintCommandParser(GetCommands()));

            foreach (var file in Templates.GetFiles("*.ptf"))
                reader.Read(file.FullName).ForEach(t => tmpDictionary.Add(t.Header["Name"], t));

            return tmpDictionary;
        }

        private void Listen()
        {
            FileInfo[] files;
            int count;

            while (true)
            {
                if (!Templates.Exists)
                    Templates.Create();

                if (!Commands.Exists)
                    Commands.Create();

                files = Templates.GetFiles();
                count = files.Count();

                if (templateFileCount != count)
                {
                    OnFilesChanged?.Invoke(Templates, files);
                    templateFileCount = count;
                }

                files = Commands.GetFiles();
                count = files.Count();

                if (commandsFileCount != count)
                {
                    OnFilesChanged?.Invoke(Commands, files);
                    commandsFileCount = count;
                }

                Thread.Sleep(5);
            }
        }

        private void DefaultFileExport()
        {
            var type = typeof(FileLoader);
            var assembly = Assembly.GetAssembly(type);
            var fileInfo = new FileInfo(Path.Combine(Commands.FullName, "Commands.json"));

            if (!fileInfo.Exists)
            {
                using (var source = assembly.GetFile($"{type.Namespace}.Commands.json"))
                using (var target = fileInfo.Create())
                    target.CopyTo(source);
            }

            fileInfo = new FileInfo(Path.Combine(Templates.FullName, "Template.ptf"));
            if (!fileInfo.Exists)
            {
                using (var source = assembly.GetFile($"{type.Namespace}.Template.ptf"))
                using (var target = fileInfo.Create())
                    target.CopyTo(source);
            }
        }
    }
}
