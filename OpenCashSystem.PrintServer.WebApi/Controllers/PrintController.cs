﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenCashSystem.PrintServer.WebApi.Model;
using OpenCashSystem.PrintServer.WebApi.Printing;

namespace OpenCashSystem.PrintServer.WebApi.Controllers
{
    [Route("[controller]")]
    public class PrintController : Controller
    {
        // GET print
        [HttpGet]
        public JsonResult GetPrinter() 
            => new JsonResult(new PrinterCollection(PrintManager.Printer));

        // POST print
        [HttpPost]
        public IActionResult Post([FromBody]Documents document)
        {
            foreach (var dataItem in document.ListOfDataItems)
            {
                if(!PrintManager.TryPrint(document.PrinterId, dataItem, document.TemplateName))
                {
                    return StatusCode(418); //PrintFailed
                }
            }

            return Ok();
        }        
    }
}
