﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OpenCashSystem.PrintServer.WebApi.Printing;

namespace OpenCashSystem.PrintServer.WebApi
{
    public class Program
    {
        static FileLoader fileLoader;

        public static void Main(string[] args)
        {
            fileLoader = new FileLoader();
            fileLoader.OnFilesChanged += FileLoaderOnFilesChanged;
            fileLoader.Start();
            PrintManager.StartListening();
            BuildWebHost(args).Run();
            PrintManager.EndListening();
        }

        private static void FileLoaderOnFilesChanged(object sender, FileInfo[] e)
        {
            PrintManager.Templates.Clear();
            fileLoader.GetTemplates().ForEach(t =>
                PrintManager.Templates.TryAdd(t.Header["Name"], t)
            );
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
