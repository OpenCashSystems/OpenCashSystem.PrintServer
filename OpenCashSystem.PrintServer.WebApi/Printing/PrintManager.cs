﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ThermPrint;

namespace OpenCashSystem.PrintServer.WebApi.Printing
{
    /// <summary>
    /// Handle printer
    /// </summary>
    public static class PrintManager
    {
        public static Dictionary<int, NetworkPrinter> Printer
        {
            get
            {
                var tmpDictionary = new Dictionary<int, NetworkPrinter>();

                lock (mainLog)
                    printerList.ForEach(p => tmpDictionary.Add(p.ID, p));

                return tmpDictionary;
            }
        }
        public static ConcurrentDictionary<string, PrintTemplate> Templates;

        private static List<NetworkPrinter> printerList;
        private static NetworkPrintListener listener;
        private static object templateLog;
        private static object mainLog;

        static PrintManager()
        {
            mainLog = new object();
            printerList = new List<NetworkPrinter>();
            listener = new NetworkPrintListener();
            Templates = new ConcurrentDictionary<string, PrintTemplate>();
            templateLog = new object();

            listener.OnNetworkPrintersUpdate += ListenerOnNetworkPrintersUpdate;
        }

        public static void StartListening() => listener.Start();

        public static void EndListening() => listener.Stop();

        internal static bool TryPrint(int printerId, Dictionary<string, string> dataItems, string template)
        {
            if (string.IsNullOrWhiteSpace(template))
                return false;

            if (!Templates.TryGetValue(template, out PrintTemplate printTemplate))
                return false;

            if (Printer.TryGetValue(printerId, out NetworkPrinter printer))
                return printer.TryPrint(dataItems, printTemplate);

            return false;
        }

        private static void ListenerOnNetworkPrintersUpdate(object sender, NetworkPrinter[] e)
        {
            lock (mainLog)
            {
                printerList.Clear();
                printerList.AddRange(e);
            }
        }
    }
}
