﻿using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace OpenCashSystem.PrintServer
{
    public abstract class BaseClient
    {
        public byte[] PublicKey { get; set; }

        protected readonly Socket Socket;
        protected readonly SocketAsyncEventArgs ReceiveArgs;

        private byte readSendQueueIndex;
        private byte nextSendQueueWriteIndex;
        private bool sending;

        private readonly SocketAsyncEventArgs sendArgs;

        private readonly (byte[] data, int len)[] sendQueue;
        private byte[] key;
        private readonly object sendLock;

        private AsymmetricCipherKeyPair keyPair;
        private IBasicAgreement basicAgreement;
        private ECDomainParameters domainParameters;

        protected BaseClient(Socket socket)
        {
            sendQueue = new(byte[] data, int len)[256];
            sendLock = new object();

            Socket = socket;
            Socket.NoDelay = true;

            ReceiveArgs = new SocketAsyncEventArgs();
            ReceiveArgs.Completed += OnReceived;
            ReceiveArgs.SetBuffer(System.Buffers.ArrayPool<byte>.Shared.Rent(2048), 0, 2048);

            sendArgs = new SocketAsyncEventArgs();
            sendArgs.Completed += OnSent;

            InitCrypthography();

        }

        public void Start()
        {
            while (true)
            {
                if (Socket.ReceiveAsync(ReceiveArgs))
                    return;

                ProcessInternal(ReceiveArgs.Buffer, ReceiveArgs.BytesTransferred);
            }
        }

        public void Send(byte[] data, int len)
        {
            lock (sendLock)
            {
                if (sending)
                {
                    sendQueue[nextSendQueueWriteIndex++] = (data, len);
                    return;
                }

                sending = true;
            }

            SendInternal(data, len);

        }

        public void GeneratePrivateKey(byte[] publicKey)
        {
            var pup = new ECPublicKeyParameters(
                domainParameters.Curve.DecodePoint(publicKey), domainParameters);

            key = basicAgreement.CalculateAgreement(pup).ToByteArray();
        }

        protected abstract void ProcessInternal(byte[] receiveArgsBuffer, int receiveArgsCount);

        private void SendInternal(byte[] data, int len)
        {
            while (true)
            {
                sendArgs.SetBuffer(data, 0, len);

                if (Socket.SendAsync(sendArgs))
                    return;

                System.Buffers.ArrayPool<byte>.Shared.Return(data);

                lock (sendLock)
                {
                    if (readSendQueueIndex < nextSendQueueWriteIndex)
                    {
                        (data, len) = sendQueue[readSendQueueIndex++];
                    }
                    else
                    {
                        nextSendQueueWriteIndex = 0;
                        readSendQueueIndex = 0;
                        sending = false;
                        return;
                    }
                }
            }
        }

        private void OnSent(object sender, SocketAsyncEventArgs e)
        {
            byte[] data;
            int len;

            System.Buffers.ArrayPool<byte>.Shared.Return(e.Buffer);

            lock (sendLock)
            {
                if (readSendQueueIndex < nextSendQueueWriteIndex)
                {
                    (data, len) = sendQueue[readSendQueueIndex++];
                }
                else
                {
                    nextSendQueueWriteIndex = 0;
                    readSendQueueIndex = 0;
                    sending = false;
                    return;
                }
            }

            SendInternal(data, len);
        }

        private void OnReceived(object sender, SocketAsyncEventArgs e)
        {
            ProcessInternal(e.Buffer, e.BytesTransferred);

            while (true)
            {
                if (Socket.ReceiveAsync(ReceiveArgs))
                    return;

                ProcessInternal(ReceiveArgs.Buffer, ReceiveArgs.BytesTransferred);
            }
        }

        private void InitCrypthography()
        {
            var nameCurved = ECNamedCurveTable.GetByName("prime192v1");
            domainParameters = new ECDomainParameters(nameCurved.Curve, nameCurved.G, nameCurved.N, nameCurved.H);
            var keyGen = GeneratorUtilities.GetKeyPairGenerator("ECDH");
            var paraGen = new DHParametersGenerator();

            paraGen.Init(256, 30, new SecureRandom());
            var keyParamerters = new ECKeyGenerationParameters(domainParameters, new SecureRandom());
            keyGen.Init(keyParamerters);

            keyPair = keyGen.GenerateKeyPair();

            basicAgreement = AgreementUtilities.GetBasicAgreement("ECDH");
            basicAgreement.Init(keyPair.Private);

            var pub = (ECPublicKeyParameters)keyPair.Public;
            PublicKey = pub.Q.GetEncoded(true);
        }

    }
}
