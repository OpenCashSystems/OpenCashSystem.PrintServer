﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace OpenCashSystem.PrintServer
{
    class Server
    {
        private Socket socket;
        private List<ConnectedClient> connectedClients;
        private readonly object lockObj;

        public Server() : base()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            lockObj = new object();
        }

        public void Start(IPAddress address, int port)
        {
            connectedClients = new List<ConnectedClient>();
            socket.Bind(new IPEndPoint(address, port));
            socket.Listen(1024);
            socket.BeginAccept(OnClientAccepted, null);
        }
        public void Start(string host, int port)
        {
            var address = Dns.GetHostAddresses(host).FirstOrDefault(
                a => a.AddressFamily == socket.AddressFamily);

            Start(address, port);
        }

        private void OnClientAccepted(IAsyncResult ar)
        {
            var tmpSocket = socket.EndAccept(ar);
            socket.BeginAccept(OnClientAccepted, null);
            tmpSocket.NoDelay = true;

            var client = new ConnectedClient(tmpSocket);
            client.Start();

            lock (lockObj)
                connectedClients.Add(client);
        }
    }
}
