﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Loader;
using System.Threading;

namespace OpenCashSystem.PrintServer
{
    class Program
    {
        private static ManualResetEvent manualReset;
        private static Server server;

        static void Main(string[] args)
        {
            AssemblyLoadContext.Default.Unloading += Unloading;
            Console.CancelKeyPress += (s, e) => Console.WriteLine("Exit service...");

            server = new Server();

            server.Start(IPAddress.Any, 44444);

            manualReset.WaitOne();

        }

      

        private static void Unloading(AssemblyLoadContext obj)
        {
            Console.WriteLine("Unloading...");
            Console.Write("Set Resetevent");
            manualReset?.Set();
            Console.WriteLine("...");
        }
    }
}
