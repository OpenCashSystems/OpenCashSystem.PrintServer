﻿using CommandManagementSystem.Attributes;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;

namespace OpenCashSystem.PrintServer.Commands
{
    public static class SystemCommands
    {
        public static dynamic PingPong(object[] args)
        {
            var buffer = ArrayPool<byte>.Shared.Rent(4);
            Encoding.UTF8.GetBytes("-PONG", 0, 4, buffer, 0);
            Send(buffer, 4);

            return null;
        }
    }
}
