﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            GetJson();
            return;

            var client = new HttpClient
            {
                //BaseAddress = new Uri("http://192.168.63.1:5000")
                BaseAddress = new Uri("http://localhost:53321")
            };

            var message = (StringContent)client.GetAsync("print").Result.Content;

            //client.PostAsync("print", new StringContent(
            //    JsonConvert.SerializeObject(
            //        new Document()
            //        {
            //            Name = "Test",
            //            Id = 42
            //        }),
            //    Encoding.UTF8,
            //    "application/json")).Wait();

            //using (var writer = new StreamWriter(req.GetRequestStream()))
            //{
            //    writer.Write(JsonConvert.SerializeObject(new Document() { Name = "Test", Id = 42 }));
            //}

        }

        private static void GetJson()
        {
            File.WriteAllText(@".\example_2.json", JsonConvert.SerializeObject(new Test(), Formatting.Indented));
        }

        public class Test
        {
            public List<Dictionary<string, string>> ListOfDataItems { get; set; }
            public int PrinterId { get; set; }
            public string TemplateName { get; set; }

            public Test()
            {
                ListOfDataItems = new List<Dictionary<string, string>>()
                {
                    new Dictionary<string, string>()
                    {
                        ["Test"] = "b"
                    }
                };

                PrinterId = 192168631;
                TemplateName = "Bon";

            }
        }

    }
}