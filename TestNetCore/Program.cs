﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ThermPrint;

namespace TestNetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new PrintCommandParser();
            parser.LoadFromFile(@".\Commands.json");
            var test = new TemplateReader(parser);

            var templates = test.Read(@".\example.ptf");
            
            using (var printer = new NetworkPrinter("169.254.96.65"))
            {
                printer.TryPrint(new Dictionary<string, string>()
                {
                    ["name"] = "Schnitzel",
                    ["price"] = "2.50 EUR"
                },
                templates.First());
            }
        }
    }
}
